name = input("What is your name? ")  # asks for user input, stores in variable
print("Hello " + (name.upper()))  # print text + var using method to uppercase
age = input("Please enter your age: ")  # creates a var that is a string
statement_a = "In 50 years you will be: "  # creates a var that is a string
statement_b = ". How does that make you feel?"  # creates a var thats a string
new_age = int(age) + 50  # converts string to int and adds 50
print(statement_a + str(new_age) + statement_b)  # prints a string + a string
